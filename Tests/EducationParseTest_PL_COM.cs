﻿using NUnit.Framework;
using System.Threading.Tasks;

namespace Tests {
	[TestFixture]
	public class EducationParseTests_PL_COM {
		[Test]
		public async Task CalendarParseTest_PL_COM() {
			var url = "http://learn.comparic.pl/lessons/xml";
			var feed = await WebService.Shared.GetFeed<EducationEndpoint>(url);
			System.Console.WriteLine("[" + url + "] Feed length:" + feed.Length);
			Assert.IsNotEmpty(feed);

			var parsedFeed = WebService.Shared.ParseFeed<EducationEndpoint>(feed);
			System.Console.WriteLine("[" + url + "] Parsed feed length:" + parsedFeed.Count);
			Assert.GreaterOrEqual(parsedFeed.Count, 1);
		}
	}
}
