﻿using NUnit.Framework;
using System.Threading.Tasks;

namespace Tests {
	[TestFixture]
	public class RSSFeedParseTests_Comparic_COM {

		//[Test]
		//public async Task TestsFirstCategory_COM() {
		//	await ExecuteFeedString("http://comparic.com/category/news/feed/");
		//}

		[Test]
		public async Task TestsSecondCategory_COM() {
			await ExecuteFeedString("http://comparic.com/category/stocks/feed/");
		}

		//Turned off until further investigation
		//[Test]
		//public async Task TestsThirdCategory_COM() {
		//	await ExecuteFeedString("http://comparic.com/category/analysis/feed/");
		//}

		[Test]
		public async Task TestsFourthCategory_COM() {
			await ExecuteFeedString("http://comparic.com/category/education/feed/");
		}

		//[Test]
		//public async Task TestsFifthCategory_COM() {
		//	await ExecuteFeedString("http://comparic.com/category/forex-b2b/feed/");
		//}

		async Task ExecuteFeedString(string url) {
			var feed = await WebService.Shared.GetFeed<NewsRssEndpoint>(url);
			System.Console.WriteLine("[" + url + "] Feed length:" + feed.Length);
			Assert.IsNotEmpty(feed);

			var parsedFeed = WebService.Shared.ParseFeed<NewsRssEndpoint>(feed);
			System.Console.WriteLine("[" + url + "] Parsed feed length:" + parsedFeed.Count);
			Assert.GreaterOrEqual(parsedFeed.Count, 1);
		}
	}
}
