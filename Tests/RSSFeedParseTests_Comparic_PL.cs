﻿using NUnit.Framework;
using System.Threading.Tasks;

namespace Tests {
	[TestFixture]
	public class RSSFeedParseTests_Comparic_PL {

		[Test]
		public async Task TestsFirstCategory_PL() {
			await ExecuteFeedString("https://comparic.pl/category/aktualnosci/feed/");
		}

		[Test]
		public async Task TestsSecondCategory_PL() {
			await ExecuteFeedString("https://comparic.pl/category/gpw/feed/");
		}

		[Test]
		public async Task TestsThirdCategory_PL() {
			await ExecuteFeedString("https://comparic.pl/category/polecamy/feed/");
		}

		[Test]
		public async Task TestsFourthCategory_PL() {
			await ExecuteFeedString("https://comparic.pl/category/forex-b2b/feed/");
		}

		[Test]
		public async Task TestsFifthCategory_PL() {
			await ExecuteFeedString("https://comparic.pl/category/life/feed/");
		}

		async Task ExecuteFeedString(string url) {
			var feed = await WebService.Shared.GetFeed<NewsRssEndpoint>(url);
			System.Console.WriteLine("[" + url + "] Feed length:" + feed.Length);
			Assert.IsNotEmpty(feed);

			var parsedFeed = WebService.Shared.ParseFeed<NewsRssEndpoint>(feed);
			System.Console.WriteLine("[" + url + "] Parsed feed length:" + parsedFeed.Count);
			Assert.GreaterOrEqual(parsedFeed.Count, 30);
		}
	}
}
