﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Tests {
	public interface IWebService {
		Task<string> GetFeed(string url);
		List<IParsable> ParseFeed(string feed);
	}
}

