﻿namespace Tests {
	public class SingleNews : IParsable {
		public int ID { get; set; }
		public string PublicationDate { get; set; }
		public string ImageUrl { get; set; }
		public string Guid { get; set; }
		public string Title { get; set; }
		public string ArticleUrl { get; set; }
		public string NumberOfComments { get; set; }
		public string CommentsLink { get; set; }
		public bool IsABanner { get; set; }
		public string CategoryIndex { get; set; }
	}
}