﻿using System.Collections.Generic;

namespace Tests {
	public class EducationArticle {
		public string PublicationDate { get; set; }
		public string ImageUrl { get; set; }
		public string Title { get; set; }
		public string ArticleUrl { get; set; }
		public string NumberOfComments { get; set; }
		public string CommentsLink { get; set; }
	}

	public class EducationCategory : IParsable {
		public string Name { get; set; }
		public List<EducationCourse> Courses { get; set; }
	}

	public class EducationCourse : IParsable {
		public string Name { get; set; }
		public string Description { get; set; }
		public List<MediaItem> MediaItems { get; set; }
		public string ImagePath { get; set; }
	}

	public class MediaItem : IParsable {
		public string MediaPath { get; set; }
		public string MediaName { get; set; }
		public string MediaDescription { get; set; }
		public string MediaMime { get; set; }
	}
}
