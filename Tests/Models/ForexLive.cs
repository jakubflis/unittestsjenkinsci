﻿namespace Tests {
	public class SingleForexLiveItem : IParsable {
		public int ID { get; set; }
		public string PublicationDate { get; set; }
		public string Description { get; set; }
		public string Title { get; set; }
		public string ImageUrl { get; set; }
		public string Url { get; set; }
	}
}