﻿using System;
namespace Tests {
	public class CalendarEvent : IParsable {
		public int ID { get; set; }
		public string Title { get; set; }
		public string Link { get; set; }
		public string ImageUrl { get; set; }
		public string RegistrationUrl { get; set; }
		public DateTime Date { get; set; }
		public string DateString { get; set; }
	}
}
