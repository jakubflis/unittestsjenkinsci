﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Tests {
	public class EducationEndpoint : IWebService {

		public async Task<string> GetFeed(string url) {
			string result = null;
			ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

			using (var client = new HttpClient()) {
				try {
					result = await client.GetStringAsync(new Uri(url));
				} catch (Exception e) {
					Console.WriteLine(e);
				}
			}

			return result;
		}

		public List<IParsable> ParseFeed(string feed) {
			var rssFeed = XDocument.Load(new StringReader(feed));
			IEnumerable<IParsable> eduCategories = new List<IParsable>();

			XElement generalElement = rssFeed
				.Element("lessons");
			try {
				eduCategories = (from c in generalElement.Descendants("source")
								 select new EducationCategory {
									 Name = c.Attribute("name").Value,
									 Courses = (from cour in c.Descendants("course")
												select new EducationCourse {
													Name = cour.Attribute("name").Value,
													Description = cour.Attribute("description").Value,
													ImagePath = cour.Attribute("image_path").Value,
													MediaItems = (from media in cour.Descendants("media")
																  select new MediaItem() {
																	  MediaPath = PrepareMediaLink(media),
																	  MediaName = media.Attribute("name").Value,
																	  MediaDescription = media.Attribute("description").Value,
																	  MediaMime = media.Attribute("mime").Value
																  }).ToList()
												}).ToList()
								 }
				).ToList();
			} catch (Exception e) {
				Console.WriteLine("EUDCATION_RSS_PARSE_ERROR", e.ToString());
			}

			return eduCategories.ToList();
		}

		/* *
		 * ONLY IN OPEN BETA
		 * pending rss change to direct links
		 * */
		private string PrepareMediaLink(XElement media) {
			var rawLink = media.Attribute("path").Value;

			if (media.Attribute("mime").Value.ToLower() == "youtube") {

				return rawLink;
			}

			if (!rawLink.StartsWith("http://")) {

				return "http://learn.comparic.pl" + rawLink;
			}

			return rawLink;
		}

		public bool MyRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
			bool isOk = true;
			// If there are errors in the certificate chain, look at each error to determine the cause.
			if (sslPolicyErrors != SslPolicyErrors.None) {
				for (int i = 0; i < chain.ChainStatus.Length; i++) {
					if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown) {
						chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
						chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
						chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
						chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
						bool chainIsValid = chain.Build((X509Certificate2)certificate);
						if (!chainIsValid) {
							isOk = false;
						}
					}
				}
			}
			return isOk;
		}
	}
}
