﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;

namespace Tests {
	public class ForexLiveEndpoint : IWebService {

		public async Task<string> GetFeed(string url) {
			string result = null;
			ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

			using (var client = new HttpClient()) {
				try {
					result = await client.GetStringAsync(new Uri(url));
				} catch (Exception e) {
					Console.WriteLine(e);
				}
			}

			return result ?? "";
		}

		public List<IParsable> ParseFeed(string feed) {
			var events = Newtonsoft.Json.Linq.JArray.Parse(feed);
			var parsedEvents = new List<IParsable>();

			foreach (var singleEvent in events) {
				parsedEvents.Add(new SingleForexLiveItem {
					Description = PrepareTwitterJsonContent(singleEvent["text"].ToString()),
					Title = singleEvent["user"]["name"].ToString() ?? "",
					PublicationDate = PrepareDate(singleEvent["created_at"].ToString() ?? DateTime.Now.ToString()),
					Url = PrepareUrl(singleEvent),
					ImageUrl = PrepareMediaUrl(singleEvent)
				});
			}

			return parsedEvents;
		}

		string PrepareDate(string createdAt) {
			DateTime date = DateTime.Now;
			try {
				date = DateTime.ParseExact(createdAt, "ddd MMM dd HH:mm:ss +ffff yyyy", null);
			} catch (Exception e) {
				Console.WriteLine(createdAt);
				Console.WriteLine(e);
				return createdAt;
			}

			return (date == DateTime.Now) ? createdAt : date.ToString("ddd MMM dd HH:mm:s");
		}

		string PrepareUrl(Newtonsoft.Json.Linq.JToken singleEvent) {
			if (singleEvent["entities"]["urls"] != null && singleEvent["entities"]["urls"].Count() > 0) {
				return singleEvent["entities"]["urls"][0]["url"].ToString();
			}

			return "";
		}

		string PrepareMediaUrl(Newtonsoft.Json.Linq.JToken singleEvent) {
			if (singleEvent["entities"]["media"] != null && singleEvent["entities"]["media"].Count() > 0) {
				return singleEvent["entities"]["media"][0]["media_url"].ToString();
			}

			return "";
		}

		string PrepareTwitterJsonContent(string originalContent) {
			int index = originalContent.IndexOf("https://t.co/", StringComparison.Ordinal);

			if (index > 0) {
				originalContent = originalContent.Substring(0, index);
			}

			return originalContent;
		}

		public bool MyRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
			bool isOk = true;
			// If there are errors in the certificate chain, look at each error to determine the cause.
			if (sslPolicyErrors != SslPolicyErrors.None) {
				for (int i = 0; i < chain.ChainStatus.Length; i++) {
					if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown) {
						chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
						chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
						chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
						chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
						bool chainIsValid = chain.Build((X509Certificate2)certificate);
						if (!chainIsValid) {
							isOk = false;
						}
					}
				}
			}
			return isOk;
		}
	}
}
