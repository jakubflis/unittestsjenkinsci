﻿using System;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;

namespace Tests {
	public class CalendarEndpoint : IWebService {
		public async Task<string> GetFeed(string url) {
			string result = null;
			ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

			using (var client = new HttpClient()) {
				try {
					result = await client.GetStringAsync(new Uri(url));
				} catch (Exception e) {
					Console.WriteLine(e);
				}
			}

			return result ?? "";
		}

		public List<IParsable> ParseFeed(string feed) {
			var events = Newtonsoft.Json.Linq.JArray.Parse(feed);
			var parsedEvents = new List<IParsable>();

			for (int i = 0; i < events.Count; i++) {
				var newEvent = new CalendarEvent {
					Title = events[i]["title"].ToString(),
					ImageUrl = events[i]["image_url"].ToString(),
					RegistrationUrl = events[i]["register_url"].ToString()
				};

				DateTime newEventDate = DateTime.Now;

				try {
					newEventDate = DateTime.ParseExact(events[i]["date"].ToString(), "dd.MM.yyyy HH:mm", null);
				} catch (Exception e) {
					Console.WriteLine(newEvent.Title + " " + events[i]);
					Console.WriteLine(e);

					throw new Exception("Date Parse Exception " + events[i]);

					newEventDate = DateTime.Now;
				}

				if (newEventDate == DateTime.Now) {
					continue;
				}

				newEvent.Date = newEventDate;
				parsedEvents.Add(newEvent);
			}

			return parsedEvents;
		}

		public bool MyRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
			bool isOk = true;
			// If there are errors in the certificate chain, look at each error to determine the cause.
			if (sslPolicyErrors != SslPolicyErrors.None) {
				for (int i = 0; i < chain.ChainStatus.Length; i++) {
					if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown) {
						chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
						chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
						chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
						chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
						bool chainIsValid = chain.Build((X509Certificate2)certificate);
						if (!chainIsValid) {
							isOk = false;
						}
					}
				}
			}
			return isOk;
		}
	}
}
