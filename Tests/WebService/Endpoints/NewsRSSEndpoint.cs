﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace Tests {
	public class NewsRssEndpoint : IWebService {
		readonly XNamespace _slash = "http://purl.org/rss/1.0/modules/slash/";
		public int AdStep = 10;

		public async Task<string> GetFeed(string url) {
			string result = null;
			ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

			using (var client = new HttpClient()) {
				try {
					result = await client.GetStringAsync(new Uri(url));
				} catch (Exception e) {
					Console.WriteLine(e);
				}
			}

			return result ?? "";
		}

		public List<IParsable> ParseFeed(string feed) {
			var rssFeed = XDocument.Load(new StringReader(feed));
			var posts = new List<IParsable>();

			foreach (var item in rssFeed.Element("rss").Element("channel").Descendants("item")) {
				SingleNews newEntry = null;
				try {
					newEntry = new SingleNews {
						IsABanner = false,
						Title = item.Element("title").Value,
						Guid = item.Element("guid").Value,
						ImageUrl = ParseImg(item.Element("description").ToString()),
						PublicationDate = DateTime.Parse(item.Element("pubDate").Value.Remove(item.Element("pubDate").Value.IndexOf(" +"))).ToString("dd/MM/yyyy hh:mm"),
						ArticleUrl = item.Element("link").Value,
						NumberOfComments = item.Element(_slash + "comments").Value,
						CommentsLink = item.Element("comments").Value
					};
				} catch (Exception e) {
					//Just don't add the post if something went wrong
					System.Diagnostics.Debug.WriteLine(e);
					continue;
				}

				if (newEntry != null) {
					posts.Add(newEntry);
				}
			}

			return posts.ToList();
		}

		string ParseImg(string element) {
			var result = Regex.Match(element, "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.None).Groups[1].Value;

			return result ?? "";
		}

		public bool MyRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
			bool isOk = true;
			// If there are errors in the certificate chain, look at each error to determine the cause.
			if (sslPolicyErrors != SslPolicyErrors.None) {
				for (int i = 0; i < chain.ChainStatus.Length; i++) {
					if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown) {
						chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
						chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
						chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
						chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
						bool chainIsValid = chain.Build((X509Certificate2)certificate);
						if (!chainIsValid) {
							isOk = false;
						}
					}
				}
			}
			return isOk;
		}
	}
}
