﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Tests {
	public class WebService {
		public static WebService Shared = new WebService();

		public async Task<string> GetFeed<T>(string url) where T : IWebService, new() {
			return await new T().GetFeed(url);
		}

		public List<IParsable> ParseFeed<T>(string feed) where T : IWebService, new() {
			return new T().ParseFeed(feed);
		}
	}
}

