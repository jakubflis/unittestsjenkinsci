﻿using NUnit.Framework;

using System.Threading.Tasks;

namespace Tests {
	[TestFixture]
	public class ForexLiveParseTests_COM_PL {
		[Test]
		public async Task CalendarParseTest_PL() {
			var url = "https://comparic.pl/apitwitter/";
			var feed = await WebService.Shared.GetFeed<ForexLiveEndpoint>(url);
			System.Console.WriteLine("[" + url + "] Feed length:" + feed.Length);
			Assert.IsNotEmpty(feed);

			var parsedFeed = WebService.Shared.ParseFeed<ForexLiveEndpoint>(feed);
			System.Console.WriteLine("[" + url + "] Parsed feed length:" + parsedFeed.Count);
			Assert.GreaterOrEqual(parsedFeed.Count, 1);
		}

		[Test]
		public async Task CalendarParseTest_COM() {
			var url = "https://comparic.pl/apientwitter/";
			var feed = await WebService.Shared.GetFeed<ForexLiveEndpoint>(url);
			System.Console.WriteLine("[" + url + "] Feed length:" + feed.Length);
			Assert.IsNotEmpty(feed);

			var parsedFeed = WebService.Shared.ParseFeed<ForexLiveEndpoint>(feed);
			System.Console.WriteLine("[" + url + "] Parsed feed length:" + parsedFeed.Count);
			Assert.GreaterOrEqual(parsedFeed.Count, 1);
		}
	}
}
