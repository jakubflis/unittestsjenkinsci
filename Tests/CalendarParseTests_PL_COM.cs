﻿using NUnit.Framework;

using System.Threading.Tasks;

namespace Tests {
	[TestFixture]
	public class CalendarParseTests_PL_COM {
		[Test]
		public async Task CalendarParseTest_PL_COM() {
			var url = "https://www.comparic.pl/api/calendar/";
			var feed = await WebService.Shared.GetFeed<CalendarEndpoint>(url);
			System.Console.WriteLine("[" + url + "] Feed length:" + feed.Length);
			Assert.IsNotEmpty(feed);

			var parsedFeed = WebService.Shared.ParseFeed<CalendarEndpoint>(feed);
			System.Console.WriteLine("[" + url + "] Parsed feed length:" + parsedFeed.Count);
			Assert.GreaterOrEqual(parsedFeed.Count, 1);
		}
	}
}
